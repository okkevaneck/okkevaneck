#!/usr/bin/env bash

source venv/bin/activate

if [[ -z $1 ]] || [[ $1 = "flask" ]] || [[ $1 = "backend" ]]; then
    FLASK_APP=run.py FLASK_DEBUG=1 flask run
elif [[ $1 = "npm" ]] || [[ $1 = "frontend" ]]; then
    cd frontend
    npm run dev
elif [[ $1 = "build" ]]; then
    cd frontend
    npm run build
fi

